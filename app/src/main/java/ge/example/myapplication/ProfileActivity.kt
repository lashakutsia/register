package ge.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {
    private lateinit var buttonlogout: Button
    private lateinit var texView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        init()
        registerListener()
        texView.text = FirebaseAuth.getInstance().currentUser?.uid
    }

    private fun init() {

        buttonlogout = findViewById(R.id.buttonlogout)
        texView = findViewById(R.id.textView)
    }

    private fun registerListener () {
        buttonlogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

    }

}