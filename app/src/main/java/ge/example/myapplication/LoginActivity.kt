package ge.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonlogin: Button
    private lateinit var buttonregister: Button




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        init()
        registerListeners()
    }
    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonlogin = findViewById(R.id.buttonlogin)
        buttonregister = findViewById(R.id.buttonregister)
    }

    private fun registerListeners() {
        buttonregister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)

        }

        buttonlogin.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if(email.isEmpty() || !(email.contains("@")) || password.isEmpty() || password.length < 9 || !(password.contains("1") ) || !(password.contains("2") ) || !(password.contains("3") ) || !(password.contains("4") ) || !(password.contains("5") ) || !(password.contains("6") ) || !(password.contains("7") ) || !(password.contains("8") ) || !(password.contains("0") ) || !(password.contains("9") ) || !(password.contains(".") )   ) {
                Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                return@setOnClickListener

            }
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        gotoProfile()
                    } else {
                        Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }
        }

    }
    private fun gotoProfile() {
        startActivity(Intent(this, ProfileActivity::class.java))
        finish()
    }

}